from creational_patterns.builder.builders import Printer
from creational_patterns.builder.directors import SpendingProcessor


def builder_client():
    builder = Printer()
    director = SpendingProcessor(builder)
    director.construct()

if __name__ == '__main__':
    builder_client()