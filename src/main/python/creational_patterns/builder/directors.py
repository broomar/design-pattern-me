from creational_patterns.builder.interfaces import Builder, Director
import pandas as pd
from creational_patterns.builder.models import Purchase, Return


class SpendingProcessor(Director):
    """
    Processes my spending ... and derives information
    """

    def __init__(self, builder:Builder):
        self.builder = builder

    def construct(self, csv_file="/Users/oeid/Downloads/Chase4415_Activity_20190122.CSV") -> Builder:

        df = pd.read_csv(csv_file)
        purchases = list(map(
            lambda x: Purchase(**x),
            df[df["Type"]=="Sale"].drop(["Type", "Post Date"], axis=1).rename(columns={
                "Amount":"total",
                "Description":"seller",
                "Trans Date":"soldOn",
            }).to_dict("records")
        ))
        returns = list(map(
            lambda x: Return(**x),
            df[df["Type"] == "Return"].drop(["Type", "Post Date"], axis=1).rename(columns={
                "Amount": "total",
                "Description": "returnedTo",
                "Trans Date": "returnedOn",
            }).to_dict("records")
        ))

        for purchase in purchases:
            self.builder.append_purchase(purchase)

        for _return in returns:
            self.builder.append_return(_return)

        return self.builder