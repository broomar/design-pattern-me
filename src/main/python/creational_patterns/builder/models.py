import attr
import arrow


@attr.s(frozen=True)
class Purchase(object):
    total = attr.ib(type=float)
    seller = attr.ib(type=str)
    soldOn = attr.ib(type=arrow.Arrow, converter=lambda x: arrow.get(x, ["M/D/YY", "MM/DD/YY"]) if isinstance(x, str) else x)


@attr.s(frozen=True)
class Return(object):
    total = attr.ib(type=float)
    returnedTo = attr.ib(type=str)
    returnedOn = attr.ib(type=arrow.Arrow, converter=lambda x: arrow.get(x, ["M/D/YY", "MM/DD/YY"]) if isinstance(x, str) else x)

