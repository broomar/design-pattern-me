import abc

from creational_patterns.builder.models import Return, Purchase


class Director(abc.ABC):

    @abc.abstractmethod
    def construct(self):
        pass


class Builder(abc.ABC):

    @abc.abstractmethod
    def append_purchase(self, purchase:Purchase):
        pass

    @abc.abstractmethod
    def append_return(self, returned:Return):
        pass
