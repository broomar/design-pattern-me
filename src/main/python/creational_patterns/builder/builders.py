from creational_patterns.builder.interfaces import Builder, Director
from creational_patterns.builder.models import Purchase, Return
import attr


class Printer(Builder):

    def append_purchase(self, purchase:Purchase):
        print(attr.asdict(purchase))

    def append_return(self, returned:Return):
        print(attr.asdict(returned))


class WeeklyTotalSpending(Builder):

    def __init__(self):
        self.weekly_purchases = []
        self.weekly_returns = []

    def append_purchase(self, purchase:Purchase):
        pass

    def append_return(self, returned:Return):
        pass


class RecurringPurchasesBuilder(Builder):
    def append_purchase(self, purchase:Purchase):
        pass

    def append_return(self, returned:Return):
        pass