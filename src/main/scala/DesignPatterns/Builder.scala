package DesignPatterns


trait ActivityImportBuilder[A] {
  def append_chase_credit_transactions(accountName:String, cardNumber:String, path:String): ActivityImportBuilder[A]
  def append_chase_debit_transactions(accountName:String, cardNumber:String, path:String): ActivityImportBuilder[A]
  //  def append_citi_credit_transactions(accountName:String, cardNumber:String, path:String): ActivityImportBuilder[A]
  def get(): A
}

//trait TransactionBuilder[A] {
//  def append_purchase(builder: TransactionBuilder[A]): TransactionBuilder[A]
//  def append_return: TransactionBuilder[A]
//  def get(builder: TransactionBuilder[A]) : A
//}

trait Director[A] {
  def construct: A
}