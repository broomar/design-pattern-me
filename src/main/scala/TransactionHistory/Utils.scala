package TransactionHistory

import org.joda.time.format.ISODateTimeFormat

import com.github.nscala_time.time.Imports._
import scalaz._

object Utils {

  /*
   Returns ISO Timestamp in UTC ...
   */
  def datetime_to_string(date:DateTime):String = {
    return date.toDateTime(DateTimeZone.UTC).toString(ISODateTimeFormat.dateTime())
  }

  /*
   Returns UTC DateTime from String with Default Pattern ...
   */
  def datetime_from_string(date:String, pattern:Maybe[String]):DateTime = {
    return date.toDateTime(pattern.getOrElse("MM/dd/yyyy")).toDateTime(DateTimeZone.UTC)
  }

  def timestamp_from_datetime(date:DateTime): java.sql.Timestamp = {
    return new java.sql.Timestamp (date.getMillis)
  }

  def timestamp_to_datetime(ts:java.sql.Timestamp): DateTime = {
    return new DateTime(ts, DateTimeZone.UTC);
  }

  def timestamp_from_string(date:String): java.sql.Timestamp = {
    return timestamp_from_datetime(datetime_from_string(date, Maybe.empty[String]))
  }

}
