package TransactionHistory

import DesignPatterns.Director
import TransactionHistory.BuilderImplementations.ActivityProcessor
import org.apache.commons.lang.StringEscapeUtils
import scalaz.{Maybe, std}
import scalaz.Maybe._

object DirectorImplementations {

  /** Gets a decrypted resource from src/test/resources ...
    *
    * @param relative_path
    * @return
    */
  def get_decrypted_resource(relative_path:String): String = {
    return getClass.getResource(s"/decrypted/${relative_path}").getPath
  }

  object ActivityCountDirector extends Director[Int]{
    def construct(): Int = {
      val transactions =
        ActivityProcessor(Just(List[Transaction]()): Maybe[List[Transaction]])
          .append_chase_credit_transactions("CHASE FREEDOM", "4415", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase4415_Activity20170131_20180101_20190131.CSV")
          .append_chase_credit_transactions("CHASE FREEDOM", "4415", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase4415_Activity20180101_20190101_20190131.CSV")
          .append_chase_credit_transactions("CHASE AMAZON", "0779", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase0779_Activity20170131_20190131_20190131.CSV")
          .append_chase_debit_transactions("CHASE DEBIT", "1756", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase1756_Activity_20190131.CSV")
          .get()
      // Explain the differnce between what the director returns and builder's return value
      return transactions.length
    }
  }

  case class Vendor(name:String, store_number:Maybe[Int])

  object UniqueVendorsInActivityDirector extends Director[Set[Vendor]]{

// https://alvinalexander.com/scala/how-cast-string-to-int-in-scala-string-int-conversion
//    https://alvinalexander.com/java/jwarehouse/scalaz-7.3/core/src/main/scala/scalaz/Maybe.scala.shtml
//    https://www.scala-lang.org/api/2.12.3/scala/util/matching/Regex.html
//    https://stackoverflow.com/questions/5778657/how-do-i-convert-an-arraystring-to-a-setstring
//    https://www.scala-lang.org/api/2.12.3/scala/util/matching/Regex.html
//      - [ ] Figure out how to get the regex to work right ...
//    - [ ] Figure out how to get the Option -> Maybe iso morphism working ...

    def vendor_from_description(description:String): Vendor = {
      val potential_store_pattern = raw"[^a-zA-Z]*(\d+)[^a-zA-Z]*".r
      // TODO ... Store Number Extraction Model!
      return Vendor(
        description,
        potential_store_pattern.findFirstIn(description).map(Maybe.just(_)).getOrElse(Maybe.empty[String]).map(_.toInt)
      )
    }

    def construct(): Set[Vendor] = {
      val transactions =
        ActivityProcessor(Just(List[Transaction]()): Maybe[List[Transaction]])
          .append_chase_credit_transactions("CHASE FREEDOM", "4415", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase4415_Activity20170131_20180101_20190131.CSV")
          .append_chase_credit_transactions("CHASE FREEDOM", "4415", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase4415_Activity20180101_20190101_20190131.CSV")
          .append_chase_credit_transactions("CHASE AMAZON", "0779", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase0779_Activity20170131_20190131_20190131.CSV")
          .append_chase_debit_transactions("CHASE DEBIT", "1756", "/Users/oeid/Workspace/git/design-pattern-me/src/main/resources/decrypted/Chase1756_Activity_20190131.CSV")
          .get()
      return transactions.map(x => StringEscapeUtils.unescapeHtml(x.description)).toSet.map(vendor_from_description)
    }
  }

}

//Payments to other cards ..
//Square Payments ...
//Paypal Payments ...
//Get rid of white space ...
//Extract store numbers ...

// ML Model for if something is a store number or not ...
// Use my transactions as training data ...
