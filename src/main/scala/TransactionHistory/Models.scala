package TransactionHistory

import com.github.nscala_time.time.Imports._
import java.sql.Timestamp

object Models {
  case class RawChaseCreditActivityRecord(transType: String, transDate: String, postDate: String, category: String, description: String, amount: String)
  case class RawChaseDebitActivityRecord(details: String, postDate: String, description: String, amount: String, transType: String, balance:String, checkOrSlipNum:String)
}

sealed trait Transaction {
  def description: String
  def amount: Float
  def date: Timestamp
}

case class CreditPurchase(accountName:String, cardNumber: String, date: Timestamp, description: String, amount: Float) extends Transaction
case class CreditPayment(accountName:String, cardNumber: String, date: Timestamp , description: String, amount: Float) extends Transaction
case class CreditReturn(accountName:String, cardNumber: String, date: Timestamp , description: String, amount: Float) extends Transaction

case class DebitDeposit(accountName:String, cardNumber: String, date: Timestamp , description: String, amount: Float) extends Transaction
case class DebitPayment(accountName:String, cardNumber: String, date: Timestamp , description: String, amount: Float) extends Transaction
case class DebitIncome(accountName:String, cardNumber: String, date: Timestamp, description: String, amount: Float) extends Transaction

