package TransactionHistory

import org.apache.spark.sql.Encoders
import TransactionHistory.Models._

object TransactionEncoders {
  implicit val RawChaseActivityRecordEncoder = Encoders.product[RawChaseCreditActivityRecord]
  implicit val RawChaseDebitActivityRecordEncoder = Encoders.product[RawChaseDebitActivityRecord]
  implicit val CreditPurchaseEncoder = Encoders.product[CreditPurchase]
  implicit val CreditPaymentEncoder = Encoders.product[CreditPayment]
  implicit val CreditReturnEncoder = Encoders.product[CreditReturn]
  implicit val DebitPaymentEncoder = Encoders.product[DebitPayment]
  implicit val DebitDepositEncoder = Encoders.product[DebitDeposit]
  implicit val DebitIncomeEncoder = Encoders.product[DebitIncome]

}
