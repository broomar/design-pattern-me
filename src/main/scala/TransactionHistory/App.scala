package TransactionHistory

import TransactionHistory.DirectorImplementations.UniqueVendorsInActivityDirector
import org.apache.spark.sql.SparkSession


object TestApp extends App {
  implicit val spark = SparkSession.builder().master("local").appName("LocalMode").getOrCreate()
//  System.out.println(s"Total Transactions ... : ${ActivityCountDirector.construct()}")
  System.out.println(s"Unique Vendors ... : ${UniqueVendorsInActivityDirector.construct()}")

//  Leventhies similarity ... group similar names ... excluding numebers ...

//  If trying to do a similarity matrix ...
//    https://github.com/apache/spark/blob/master/examples/src/main/scala/org/apache/spark/examples/mllib/CosineSimilarity.scala
//    https://databricks.com/blog/2014/10/20/efficient-similarity-algorithm-now-in-spark-twitter.html
//  https://blog.twitter.com/engineering/en_us/a/2014/all-pairs-similarity-via-dimsum.html
//  https://github.com/apache/spark/pull/336
//  https://arxiv.org/abs/1304.1467
}