package TransactionHistory

import DesignPatterns.ActivityImportBuilder
import TransactionHistory.TransactionEncoders._
import TransactionHistory.Models.{RawChaseCreditActivityRecord, RawChaseDebitActivityRecord}
import TestApp.spark
import scalaz.Maybe

object BuilderImplementations {

  case class ActivityProcessor(transactions: Maybe[List[Transaction]]) extends ActivityImportBuilder[List[Transaction]] {

    def append_chase_credit_transactions(accountName:String, cardNumber:String, path: String) : ActivityImportBuilder[List[Transaction]] = {
      val df = spark.read.option("header","true").csv(path)
      val ds = df
        .withColumnRenamed("Category", "category")
        .withColumnRenamed("Type", "transType")
        .withColumnRenamed("Transaction Date", "transDate")
        .withColumnRenamed("Post Date", "postDate")
        .withColumnRenamed("Description", "description")
        .withColumnRenamed("Amount", "amount")
        .as[RawChaseCreditActivityRecord]

      val returns = ds
        .filter(x => x.transType == "Return")
        .map(x => CreditReturn(accountName, cardNumber, Utils.timestamp_from_string(x.transDate), x.description, x.amount.toFloat))

      val sales = ds
        .filter(x => x.transType == "Sale")
        .map(x => CreditPurchase(accountName, cardNumber, Utils.timestamp_from_string(x.transDate), x.description, x.amount.toFloat))

      val payments = ds
        .filter(x => x.transType == "Payment")
        .map(x => CreditPayment(accountName, cardNumber, Utils.timestamp_from_string(x.transDate), x.description, x.amount.toFloat))

      return ActivityProcessor(
        Maybe.just[List[Transaction]](
          this.get() ++ (returns.collect.toList) ++ (sales.collect.toList) ++ (payments.collect.toList)
        )
      )
    }

    def append_chase_debit_transactions(accountName: String, cardNumber: String, path: String): ActivityImportBuilder[List[Transaction]] = {

      val df = spark.read.option("header","true").csv(path)
      val ds = df
        .withColumnRenamed("Details", "details")
        .withColumnRenamed("Posting Date", "postDate")
        .withColumnRenamed("Description", "description")
        .withColumnRenamed("Amount", "amount")
        .withColumnRenamed("Type", "transType")
        .withColumnRenamed("Balance", "balance")
        .withColumnRenamed("Check or Slip #", "checkOrSlipNum")
        .as[RawChaseDebitActivityRecord]

      val payments = ds
        .filter(x => x.details == "DEBIT")
        .map(x => DebitPayment(accountName, cardNumber, Utils.timestamp_from_string(x.postDate), x.description, x.amount.toFloat))

      val income = ds
        .filter(x => x.details == "CREDIT")
        .map(x => DebitIncome(accountName, cardNumber, Utils.timestamp_from_string(x.postDate), x.description, x.amount.toFloat))

      val deposits = ds
        .filter(x => x.details == "DSLIP")
        .map(x => DebitDeposit(accountName, cardNumber, Utils.timestamp_from_string(x.postDate), x.description, x.amount.toFloat))

      return ActivityProcessor(
        Maybe.just[List[Transaction]](
          this.get() ++ (payments.collect.toList) ++ (income.collect.toList) ++ (deposits.collect.toList)
        )
      )
    }

    def get(): List[Transaction] = {
      return this.transactions.getOrElse(List[Transaction]())
    }
  }
}
