package LearningScalaSnippets

class Scratchpad {

}

// Stream Append
//scala> Stream(1,2,3,4,5).foldLeft( (List(): List[Int], Stream(): Stream[Int]) ){ case ((a, s), y) => ((y::a), s.append(Stream(y))) }
//res12: (List[Int], Stream[Int]) = (List(5, 4, 3, 2, 1),Stream(1, ?))