package LearningScalaSnippets

/**
  * Created by oeid on 3/13/17.
  */

object StringContexts {
  // implicit class JsonHelper(val sc: StringContext) extends AnyVal {	}
  // Infinite Loop:
  // println(s"${ if( x == 1) { while ( x== 1 ) { 12 }; 1 } else { 1 }  }")

  //	val dude = "aasdfasdfadfadasdf"
  //	val str = { x: String => s"My name is ${x} and I want to eat ${if (x == "jane") {10} else {20}} bananas" }
  //	println(str("aasdfasdfadfadasdf"))
  //	println(StringContext("My name is ", " and I want to eat ", " bananas").s(dude, if (dude == "jane") {10} else {20}))

  val cands = List(
    Map("itemName" -> "do you want some "),
    Map("itemName" -> "do you want some more ")
  )
  //	val templ = cands.map {
  //		gist => s"""
  //			{
  //				"key": "${gist("itemName") + "applesauce"}"
  //			}
  //			""".parse.map(x => x.spaces4)

}