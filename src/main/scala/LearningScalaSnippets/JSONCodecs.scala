package LearningScalaSnippets

import argonaut._
import Argonaut._

object Obj {

  case class OuterType[R: DecodeJson](generalField: String, innerType: R)

  object OuterType {
    implicit def OuterTypeDecodeJson[R: DecodeJson]: DecodeJson[OuterType[R]] =
      DecodeJson(
        c => for {
          generalField <- (c --\ "generalField").as[String]
          innerType <- (c --\ "innerType").as[R]
        } yield OuterType(generalField, innerType)
      )
    implicit def OuterTypeEncodeJson[R: EncodeJson]: EncodeJson[OuterType[R]] =
      EncodeJson((ir: OuterType[R]) =>
        ("generalField" := ir.generalField)
          ->: ("innerType" := ir.innerType)
          ->: jEmptyObject)
  }

  case class LocationSchema(city: String, state: String, country: String)
  object LocationSchema {
    implicit def LocationSchemaJson: CodecJson[LocationSchema] =
      casecodec3(LocationSchema.apply, LocationSchema.unapply)("CITY", "STATE", "COUNTRY")
  }

}

object SimpleApp extends App {
  import Obj._
  val is =  """{"generalField":"hi", "innerType": [{"CITY":"A CITY", "STATE": "A state", "COUNTRY": "A country"}]}"""
  println(is.parseOption.map(j => j.as[OuterType[List[LocationSchema]]].result));
}
