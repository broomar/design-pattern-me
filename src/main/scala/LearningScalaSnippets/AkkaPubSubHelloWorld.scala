package LearningScalaSnippets

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.event.{EventBus, LookupClassification}


final case class MsgEnvelope(topic: String, payload: Any)

class LookupBusImpl extends EventBus with LookupClassification {
  type Event = MsgEnvelope
  type Classifier = String
  type Subscriber = ActorRef

  // Which subscribers should the message be forwarded to ...?
  override protected def classify(event: Event): Classifier = event.topic

  // What part of the message should be forwarded to subscribers ...?
  override protected def publish(event: Event, subscriber: Subscriber): Unit = {
    subscriber ! event.payload
  }

  override protected def compareSubscribers(a: Subscriber, b: Subscriber): Int =
    a.compareTo(b)

  // How many topics do we expect to exist?
  override protected def mapSize: Int = 128

}

class Logger(id:String) extends Actor with ActorLogging {
  def receive = {
    case s: String => {
      log.info(s"Logger[${id}] -> ${s}")
    }
  }
}

object Logger {
  def props(id: String): Props = Props(new Logger(id))
}


object AkkaPubSubQuickstart extends App {

  val system: ActorSystem = ActorSystem("helloAkka")

  val actor1: ActorRef = system.actorOf(Logger.props("1"), "printerActor1")
  val actor2: ActorRef = system.actorOf(Logger.props("2"), "printerActor2")
  val actor3: ActorRef = system.actorOf(Logger.props("3"), "printerActor3")

  val lookupBus = new LookupBusImpl

  lookupBus.subscribe(actor1, "single-logger")
  lookupBus.subscribe(actor1, "double-logger")
  lookupBus.subscribe(actor2, "double-logger")
  lookupBus.subscribe(actor1, "triple-logger")
  lookupBus.subscribe(actor2, "triple-logger")
  lookupBus.subscribe(actor3, "triple-logger")

  lookupBus.publish(MsgEnvelope("single-logger", "Only one logger will write me."))
  lookupBus.publish(MsgEnvelope("double-logger", "Only two loggers will write me."))
  lookupBus.publish(MsgEnvelope("triple-logger", "All Three Loggers will write me."))

  system.terminate()

}